// SPDX-FileCopyrightText: Copyright 2024 xlogic <https://xlogic.dev> and contributors to the project
// SPDX-License-Identifier: Apache-2.0
// SPDX-FileContributor: 2024 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

use serde_json;
use std::fmt::{Display, Formatter, Result};
use std::io;

#[derive(Debug)]
pub enum Error {
    MissingExecutable,
    MissingCommand,
    MissingOutput,
    MissingWork,
    UnsupportedCommand(String),
    FileCreate(io::Error),
    FileWrite(io::Error),
    FileRead(io::Error, String),
    CreateDirectory(io::Error, String),
    CommandFailed(io::Error),
    CommandTerminated,
    CommandExit(i32),
    JSON(serde_json::Error),
    CurrentWorkingDirectory(io::Error),
    PathEncoding,
}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter) -> Result {
        match self {
            Error::MissingExecutable => write!(f, "missing executable: xlogic-compiler"),
            Error::MissingCommand => write!(f, "missing required first argument: <command>"),
            Error::MissingOutput => write!(f, "missing output argument: -o <file>"),
            Error::MissingWork => write!(f, "missing work argument: -work <name>"),
            Error::UnsupportedCommand(command) => {
                write!(f, "unsupported command: {command}")
            }
            Error::FileCreate(e) => write!(f, "file creation failed with error: {e}"),
            Error::FileWrite(e) => write!(f, "write to file failed with error: {e}"),
            Error::FileRead(e, file) => write!(f, "cannot read from file: {file}: {e}"),
            Error::CreateDirectory(e, dir) => write!(f, "cannot create directory: {e}: {dir}"),
            Error::CommandFailed(e) => write!(f, "command executed with error: {e}"),
            Error::CommandTerminated => write!(f, "command terminated by signal"),
            Error::CommandExit(code) => write!(f, "command exited with code: {code}"),
            Error::JSON(e) => write!(f, "JSON error: {e}"),
            Error::CurrentWorkingDirectory(e) => {
                write!(f, "cannot get current working directory: {e}")
            }
            Error::PathEncoding => write!(f, "cannot encode path to valid UTF-8"),
        }
    }
}
