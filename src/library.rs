// SPDX-FileCopyrightText: Copyright 2024 xlogic <https://xlogic.dev> and contributors to the project
// SPDX-License-Identifier: Apache-2.0
// SPDX-FileContributor: 2024 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize)]
pub struct Library {
    pub name: String,
    pub directory: String,
}
