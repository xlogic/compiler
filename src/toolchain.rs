// SPDX-FileCopyrightText: Copyright 2024 xlogic <https://xlogic.dev> and contributors to the project
// SPDX-License-Identifier: Apache-2.0
// SPDX-FileContributor: 2024 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

use crate::error::Error;
use crate::object::Object;
use crate::path;
use crate::result::Result;
use serde_json;
use std::fs::File;

pub trait Toolchain {
    fn create_object(&self, command: String, output: String, arguments: Vec<String>) -> Result {
        serde_json::to_writer(
            File::create(output).map_err(|e| Error::FileCreate(e))?,
            &Object {
                directory: path::current_working_directory()?,
                command,
                arguments,
            },
        )
        .map_err(|e| Error::JSON(e))
    }

    fn create_shared_library(
        &self,
        command: String,
        output: String,
        arguments: Vec<String>,
    ) -> Result;

    fn create_static_library(
        &self,
        command: String,
        output: String,
        arguments: Vec<String>,
    ) -> Result;

    fn create_executable(&self, command: String, output: String, arguments: Vec<String>) -> Result;
}
