// SPDX-FileCopyrightText: Copyright 2024 xlogic <https://xlogic.dev> and contributors to the project
// SPDX-License-Identifier: Apache-2.0
// SPDX-FileContributor: 2024 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

use crate::error::Error;
use crate::library::Library;
use crate::object::Object;
use crate::path;
use crate::result::Result;
use crate::toolchain::Toolchain;
use serde_json;
use std::fs::create_dir_all;
use std::fs::remove_file;
use std::fs::File;
use std::path::Path;
use std::process::Command;

pub struct VivadoSimulator {}

impl Toolchain for VivadoSimulator {
    fn create_shared_library(
        &self,
        _command: String,
        output: String,
        arguments: Vec<String>,
    ) -> Result {
        let mut work: Option<String> = None;
        let mut options: Vec<String> = Vec::with_capacity(arguments.len());
        let mut inputs: Vec<String> = Vec::with_capacity(arguments.len());
        let mut args = arguments.into_iter();

        while let Some(arg) = args.next() {
            match arg.as_str() {
                "-work" | "--work" => {
                    let next_arg = args.next().ok_or_else(|| Error::MissingWork)?;
                    work = Some(next_arg.clone());
                    options.extend([arg, next_arg]);
                }
                _ => match path::extension(&arg) {
                    "vo" | "o" | "v" | "sv" | "vhd" | "vhdl" => inputs.push(arg),
                    _ => options.push(arg),
                },
            }
        }

        let library = match work {
            Some(work) => create_library(&work)?,
            None => {
                let library = create_library(path::basename(&output))?;
                options.extend(["--work".to_string(), work_argument(&library)]);
                library
            }
        };

        for input in inputs {
            match path::extension(&input) {
                "vo" | "o" => compile(input, &library)?,
                _ => {}
            }
        }

        let file = File::create(output).map_err(|e| Error::FileCreate(e))?;
        serde_json::to_writer(file, &library).map_err(|e| Error::JSON(e))
    }

    fn create_static_library(
        &self,
        _command: String,
        _output: String,
        _arguments: Vec<String>,
    ) -> Result {
        Ok(())
    }

    fn create_executable(
        &self,
        _command: String,
        _output: String,
        _arguments: Vec<String>,
    ) -> Result {
        Ok(())
    }
}

fn create_library(work: &str) -> Result<Library> {
    let (name, directory) = match work.split_once("=") {
        Some(..) => (0.to_string(), 1.to_string()),
        _ => (
            work.to_string(),
            path::join(&path::current_working_directory()?, work),
        ),
    };

    create_dir_all(&directory).map_err(|e| Error::CreateDirectory(e, directory.clone()))?;

    Ok(Library { name, directory })
}

fn work_argument(library: &Library) -> String {
    [library.name.as_str(), library.directory.as_str()]
        .join("=")
        .to_string()
}

fn compile(input: String, library: &Library) -> Result {
    let object: Object =
        serde_json::from_reader(File::open(&input).map_err(|e| Error::FileRead(e, input))?)
            .map_err(|e| Error::JSON(e))?;

    let mut arguments: Vec<String> = Vec::with_capacity(object.arguments.len() + 2);
    let mut args = object.arguments.into_iter();

    arguments.extend(["--work".to_string(), work_argument(library)]);

    while let Some(arg) = args.next() {
        match arg.as_str() {
            "-work" | "--work" => {
                let _ = args.next();
            }
            _ => arguments.push(arg),
        }
    }

    let status = Command::new(&object.command)
        .current_dir(&object.directory)
        .args(arguments)
        .status()
        .map_err(|e| Error::CommandFailed(e))
        .and_then(|status| match status.code() {
            Some(0) => Ok(()),
            Some(code) => Err(Error::CommandExit(code)),
            None => Err(Error::CommandTerminated),
        });

    let pb = Path::new(&object.directory)
        .join(path::basename(&object.command))
        .with_extension("pb");

    if pb.exists() {
        let _ = remove_file(pb);
    }

    status
}
