// SPDX-FileCopyrightText: Copyright 2024 xlogic <https://xlogic.dev> and contributors to the project
// SPDX-License-Identifier: Apache-2.0
// SPDX-FileContributor: 2024 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

use crate::error::Error;
use crate::result::Result;
use std::env::current_dir;
use std::path::Path;
use std::path::{MAIN_SEPARATOR, MAIN_SEPARATOR_STR};

pub fn current_working_directory() -> Result<String> {
    Ok(current_dir()
        .map_err(|e| Error::CurrentWorkingDirectory(e))?
        .to_str()
        .ok_or_else(|| Error::PathEncoding)?
        .to_string())
}

pub fn extension(argument: &str) -> &str {
    argument
        .rsplit_once(MAIN_SEPARATOR)
        .unwrap_or(("", argument))
        .1
        .rsplit_once(".")
        .unwrap_or_default()
        .1
}

pub fn basename(argument: &str) -> &str {
    let name = argument
        .rsplit_once(MAIN_SEPARATOR)
        .unwrap_or(("", argument))
        .1;
    name.split_once(".").unwrap_or((name, "")).0
}

pub fn join(directory: &str, file: &str) -> String {
    [directory, file].join(MAIN_SEPARATOR_STR).to_string()
}

pub fn is_absolute(path: &str) -> bool {
    Path::new(path).is_absolute()
}

pub fn is_relative(path: &str) -> bool {
    Path::new(path).is_relative()
}

pub fn resolve(directory: &str, path: &str) -> String {
    if is_absolute(path) {
        path.to_string()
    } else {
        join(directory, path)
    }
}
