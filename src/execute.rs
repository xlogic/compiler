// SPDX-FileCopyrightText: Copyright 2024 xlogic <https://xlogic.dev> and contributors to the project
// SPDX-License-Identifier: Apache-2.0
// SPDX-FileContributor: 2024 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

use std::io::stdout;
use std::io::Write;
use std::path::MAIN_SEPARATOR;
use std::process::Command;

use crate::error::Error;
use crate::result::Result;
use crate::toolchains;

static HELP: &str = "xlogic-compiler <command> -o <output> [options] file...";

pub fn execute<Args>(mut args: Args) -> Result
where
    Args: Iterator<Item = String> + ExactSizeIterator,
{
    args.next().ok_or_else(|| Error::MissingExecutable)?;
    let command = args.next().ok_or_else(|| Error::MissingCommand)?;

    if command == "-h" || command == "--help" {
        return writeln!(stdout(), "{HELP}").map_err(|e| Error::FileWrite(e));
    }

    let mut arguments: Vec<String> = Vec::with_capacity(args.len());
    let mut output: Option<String> = None;

    while let Some(arg) = args.next() {
        if arg == "-o" {
            output = Some(args.next().ok_or_else(|| Error::MissingOutput)?);
        } else {
            arguments.push(arg);
        }
    }

    let output = match output {
        Some(output) => output,
        None => {
            return Command::new(command)
                .args(arguments)
                .status()
                .map_err(|e| Error::CommandFailed(e))
                .and_then(|status| match status.code() {
                    Some(0) => Ok(()),
                    Some(code) => Err(Error::CommandExit(code)),
                    None => Err(Error::CommandTerminated),
                })
        }
    };

    let name = command
        .rsplit_once(MAIN_SEPARATOR)
        .unwrap_or(("", command.as_str()))
        .1;

    match name {
        "xvlog" | "xvhdl" | "xelab" | "xsim" => {
            dispatch(toolchains::VivadoSimulator {}, command, output, arguments)
        }
        _ => Err(Error::UnsupportedCommand(command)),
    }
}

fn dispatch<Toolchain>(
    toolchain: Toolchain,
    command: String,
    output: String,
    arguments: Vec<String>,
) -> Result
where
    Toolchain: crate::toolchain::Toolchain,
{
    let extension = output
        .rsplit_once(MAIN_SEPARATOR)
        .unwrap_or(("", output.as_str()))
        .1
        .rsplit_once(".")
        .unwrap_or_default()
        .1;

    match extension {
        "vo" | "o" => toolchain.create_object(command, output, arguments),
        "va" => toolchain.create_static_library(command, output, arguments),
        "vso" => toolchain.create_shared_library(command, output, arguments),
        _ => toolchain.create_executable(command, output, arguments),
    }
}
