// SPDX-FileCopyrightText: Copyright 2024 xlogic <https://xlogic.dev> and contributors to the project
// SPDX-License-Identifier: Apache-2.0
// SPDX-FileContributor: 2024 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

use std::env::args;
use std::process::exit;
use xlogic_compiler::error::Error;
use xlogic_compiler::execute::execute;

fn main() {
    match execute(args()) {
        Err(Error::CommandExit(code)) => exit(code),
        Err(e) => {
            eprintln!("Error: {e}");
            exit(1);
        }
        _ => {}
    }
}
